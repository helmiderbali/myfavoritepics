package com.myfavouritepics.app.listener;

import android.support.v7.widget.RecyclerView;
import com.myfavouritepics.app.ui.fragment.StreamFragment;

/**
 * Scroll listener that load more photo when the bottom of the list is reached
 * Created by helmi on 11/2/2015.
 */
public class FlickerListScrollListener extends RecyclerView.OnScrollListener {

    private final StreamFragment streamFragment;

    public FlickerListScrollListener(StreamFragment streamFragment) {
        this.streamFragment = streamFragment;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        streamFragment.mVisibleItemCount = streamFragment.mLayoutManager.getChildCount();
        streamFragment.mTotalItemCount = streamFragment.mLayoutManager.getItemCount();
        streamFragment.mPastVisiblesItems = streamFragment.mLayoutManager.findFirstVisibleItemPosition();
        if (streamFragment.mLoading) {
            if ((streamFragment.mVisibleItemCount + streamFragment.mPastVisiblesItems) >= streamFragment.mTotalItemCount) {
                streamFragment.mLoading = false;
                streamFragment.loadImagesFromFlicker(streamFragment.mPage++);
            }
        }
    }
}
