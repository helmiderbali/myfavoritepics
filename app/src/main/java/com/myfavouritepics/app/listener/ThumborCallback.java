package com.myfavouritepics.app.listener;

import com.myfavouritepics.app.Repo.ImagePostRepository;
import com.myfavouritepics.app.global.Constants;
import com.myfavouritepics.app.ui.fragment.PhotoDialogFragment;
import com.myfavouritepics.app.utils.Utils;
import com.squareup.okhttp.ResponseBody;
import retrofit.Retrofit;

/**
 * Thumbor api service callback
 * Created by helmi on 11/2/2015.
 */
class ThumborCallback implements retrofit.Callback<ResponseBody> {
    public static final int SUCCESS = 201;
    public static final String LOCATION = "Location";
    private final PhotoDialogFragment photoDialogFragment;

    public ThumborCallback(PhotoDialogFragment photoDialogFragment) {
        this.photoDialogFragment = photoDialogFragment;
    }

    @Override
    public void onResponse(retrofit.Response<ResponseBody> response, Retrofit retrofit) {
        if (response.code() == SUCCESS) {
            ImagePostRepository imageRepo = new ImagePostRepository();
            String location = Constants.THUMBOR_SERVER + response.headers().get(LOCATION);
            String description = photoDialogFragment.mComment.getText().toString();
            imageRepo.addPhoto(location, description);
            photoDialogFragment.dismiss();
            Utils.messageDialog(photoDialogFragment.getActivity(), "Nice !",
                    "Your photo was added! refresh you list!");

        } else {
            Utils.messageDialog(photoDialogFragment.getActivity(), "Oops !",
                    "Unable to process your request!");

        }
    }

    @Override
    public void onFailure(Throwable t) {
        Utils.messageDialog(photoDialogFragment.getActivity(), "Oops !",
                "Something wen wrong , please retry later!");

    }
}
