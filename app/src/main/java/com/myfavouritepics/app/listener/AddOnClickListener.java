package com.myfavouritepics.app.listener;

import android.view.View;
import com.myfavouritepics.app.manager.ThumborApiManager;
import com.myfavouritepics.app.ui.fragment.PhotoDialogFragment;
import com.squareup.okhttp.*;

import java.io.File;

/**
 * Lister for add button click that add the photo to database and thumbor
 * Created by helmi on 11/2/2015.
 */
public class AddOnClickListener implements View.OnClickListener {
    public static final String CONTENT_DISPOSITION = "Content-Disposition";
    public static final String FORM_DATA_NAME_TYPE = "form-data; name=\"type\"";
    public static final String FORM_DATA_NAME_FILENAME = "form-data; name=\"filename\"";
    public static final String FILE_TYPE = "image/jpeg";
    public static final String FILE_NAME = "image.jpg";
    public static final String MEDIA = "media";

    private final PhotoDialogFragment photoDialogFragment;

    public AddOnClickListener(PhotoDialogFragment photoDialogFragment) {
        this.photoDialogFragment = photoDialogFragment;
        photoDialogFragment.mAdd.setClickable(false);
    }

    @Override
    public void onClick(View view) {
        //Build a multipart request to send file via multipart request
        RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                .addPart(
                        Headers.of(CONTENT_DISPOSITION, FORM_DATA_NAME_TYPE),
                        RequestBody.create(null, FILE_TYPE))
                .addPart(
                        Headers.of(CONTENT_DISPOSITION, FORM_DATA_NAME_FILENAME),
                        RequestBody.create(null, FILE_NAME))
                .addFormDataPart(MEDIA,  FILE_NAME,
                        RequestBody.create(MediaType.parse(FILE_TYPE), new File(photoDialogFragment.mCurrentPhotoPath)))
                .build();
        retrofit.Call<ResponseBody> uploadImageCallback = ThumborApiManager.getService().uploadImage(requestBody);
        uploadImageCallback.enqueue(new ThumborCallback(photoDialogFragment));
    }
}
