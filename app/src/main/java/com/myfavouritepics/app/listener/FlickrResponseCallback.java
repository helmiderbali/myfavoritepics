package com.myfavouritepics.app.listener;

import android.util.Log;
import android.view.View;
import com.myfavouritepics.app.adapter.ImageListAdapter;
import com.myfavouritepics.app.model.FlickrResponse;
import com.myfavouritepics.app.model.ImagePost;
import com.myfavouritepics.app.model.Photo;
import com.myfavouritepics.app.ui.fragment.StreamFragment;
import com.myfavouritepics.app.utils.Utils;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import java.util.List;

/**
 * Response callback for flicker api service
 * Created by helmi on 11/2/2015.
 */
public class FlickrResponseCallback implements Callback<FlickrResponse> {
    private final StreamFragment streamFragment;
    private final int pageNumber;

    public FlickrResponseCallback(StreamFragment streamFragment, int pageNumber) {
        this.streamFragment = streamFragment;
        this.pageNumber = pageNumber;
    }

    @Override
    public void onResponse(Response<FlickrResponse> response, Retrofit retrofit) {
        List<Photo> photos = response.body().getPhotos().getPhotosList();
        if (pageNumber == 1) {
            streamFragment.noItems.setVisibility(View.GONE);
            streamFragment.mPosts.clear();
            convertPhotosList(photos);
            streamFragment.mAdapter = new ImageListAdapter(streamFragment.mPosts, streamFragment.getActivity());
            streamFragment.mRecyclerView.setAdapter(streamFragment.mAdapter);

        } else {
            convertPhotosList(photos);
            streamFragment.mAdapter.notifyDataSetChanged();

        }
        streamFragment.mSwipeToRefresh.setRefreshing(false);
        streamFragment.mLoading = true;
    }

    /**
     * Convert a repsonse photo list to imagepost list
     * @param photos photos list
     */
    private void convertPhotosList(List<Photo> photos) {
        for (Photo photo : photos) {
            ImagePost post = new ImagePost();
            post.setDescription(photo.getDescription().get_content());
            post.setLink(photo.getUrl_c());
            streamFragment.mPosts.add(post);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        Utils.messageDialog(streamFragment.getActivity(), "Oops !",
                "Something wen wrong , please retry later!");
        streamFragment.mSwipeToRefresh.setRefreshing(false);
        streamFragment.mLoading = true;
    }
}
