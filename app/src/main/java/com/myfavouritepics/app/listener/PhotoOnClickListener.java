package com.myfavouritepics.app.listener;

import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import com.myfavouritepics.app.R;
import com.myfavouritepics.app.ui.fragment.PhotoDialogFragment;

/**
 * Click listner for photo that show popup menu to choose action
 * Created by helmi on 11/2/2015.
 */
public class PhotoOnClickListener implements View.OnClickListener {
    public static final String FRONT_CAMERA = "Front camera";
    public static final String LIBRARY = "Library";
    private final PhotoDialogFragment photoDialogFragment;

    public PhotoOnClickListener(PhotoDialogFragment photoDialogFragment) {
        this.photoDialogFragment = photoDialogFragment;
    }

    @Override
    public void onClick(View view) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(photoDialogFragment.getActivity(), photoDialogFragment.mPhotoView);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                String title = item.getTitle().toString();
                if (title.equals(FRONT_CAMERA)) {
                    photoDialogFragment.dispatchTakePictureIntent();

                } else if (title.equals(LIBRARY)) {
                    photoDialogFragment.dispatchSelectPictureIntent();

                }
                return true;
            }
        });
        popup.show();//showing popup menu
    }
}
