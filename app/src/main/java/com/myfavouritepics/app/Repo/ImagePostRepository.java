package com.myfavouritepics.app.Repo;

import com.myfavouritepics.app.manager.DataBaseManager;
import com.myfavouritepics.app.model.ImagePost;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Image database repositry that contains different crud actions
 * Created by helmi on 10/29/2015.
 */
public class ImagePostRepository {
    private final Realm realm;

    public ImagePostRepository() {
        realm = DataBaseManager.getService();
    }

    public void addPhoto(String link, String description) {
        realm.beginTransaction();
        ImagePost imagePost = realm.createObject(ImagePost.class); // Create a new object
        imagePost.setLink(link);
        imagePost.setDescription(description);
        realm.commitTransaction();
    }

    public RealmResults<ImagePost> getAllPhoto() {
        return realm.where(ImagePost.class).findAll();
    }
}
