package com.myfavouritepics.app.manager;

import com.myfavouritepics.app.global.Constants;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;
import retrofit.Call;
import retrofit.Retrofit;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * A singleton to manage thumbor api calls
 * Created by helmi on 10/29/2015.
 */
public class ThumborApiManager {

    public interface ThumborService {

        @POST("/image")
        Call<ResponseBody> uploadImage(@Body RequestBody pictureFile);

    }

    private static final Retrofit REST_ADAPTER = new Retrofit.Builder()
            .baseUrl(Constants.THUMBOR_SERVER)
            .build();


    private static final ThumborService THUMBOR_SERVICE = REST_ADAPTER.create(ThumborService.class);

    public static ThumborService getService() {
        return THUMBOR_SERVICE;
    }
}
