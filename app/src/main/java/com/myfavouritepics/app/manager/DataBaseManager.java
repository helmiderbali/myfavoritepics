package com.myfavouritepics.app.manager;

import io.realm.Realm;
import retrofit.Retrofit;

/**
 * A singleton to manage Database Instance
 * Created by helmi on 10/29/2015.
 */
public class DataBaseManager {

    private static final Realm REALM = Realm.getDefaultInstance();

    /**
     * Gets the database instance
     * @return database instance
     */
    public static Realm getService() {
        return REALM;
    }
}
