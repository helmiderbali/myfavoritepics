package com.myfavouritepics.app.manager;

import com.myfavouritepics.app.model.FlickrResponse;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.GET;
import retrofit.http.QueryMap;

import java.util.Map;

/**
 * A singleton to manage Flickr api calls
 * Created by helmi on 10/29/2015.
 */
public class FlickrApiManager {

    /**
     * Define api structure
     */
    public interface FlickerService {

        @GET("/services/rest/")
        Call<FlickrResponse> listTravelImages(@QueryMap Map<String, String> params);

    }

    /**
     * Rest adapter global instance
     */
    private static final Retrofit REST_ADAPTER = new Retrofit.Builder()
            .baseUrl("https://api.flickr.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    private static final FlickerService FLICKER_SERVICE = REST_ADAPTER.create(FlickerService.class);

    /**
     * Gets the api instance
     * @return api instance
     */
    public static FlickerService getService() {
        return FLICKER_SERVICE;
    }
}
