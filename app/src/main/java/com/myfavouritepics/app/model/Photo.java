package com.myfavouritepics.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Photo flickr model
 * Created by helmi on 10/30/2015.
 */
public class Photo {
    @SerializedName("id")
    private String id;
    @SerializedName("owner")
    private String owner;
    @SerializedName("secret")
    private String secret;
    @SerializedName("server")
    private String server;
    @SerializedName("farm")
    private int farm;
    @SerializedName("title")
    private String title;
    @SerializedName("ispublic")
    private int ispublic;
    @SerializedName("isfreind")
    private int isfreind;
    @SerializedName("isfamily")
    private int isfamily;
    @SerializedName("description")
    private Description description;
    @SerializedName("url_c")
    private String url_c;
    @SerializedName("height_c")
    private String height_c;
    @SerializedName("width_c")
    private String width_c;

    public Photo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public String getUrl_c() {
        return url_c;
    }

}
