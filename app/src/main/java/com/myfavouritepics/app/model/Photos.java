package com.myfavouritepics.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Photos flickr model
 * Created by helmi on 10/30/2015.
 */
public class Photos {
    @SerializedName("page")
    private int page;
    @SerializedName("pages")
    private int pages;
    @SerializedName("perpage")
    private int perpage;
    @SerializedName("total")
    private String total;
   @SerializedName("photo")
    private List<Photo> photo;

    public Photos() {
    }

    public List<Photo> getPhotosList() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }
}
