package com.myfavouritepics.app.model;

import com.google.gson.annotations.SerializedName;


/**
 * Response flickr model
 * Created by helmi on 10/30/2015.
 */
public class FlickrResponse {
    @SerializedName("photos")
    private Photos photos;
    @SerializedName("stat")
    private String stat;

    public FlickrResponse() {
    }

    public Photos getPhotos() {
        return photos;
    }

}

