package com.myfavouritepics.app.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Image post model used for database and api calls
 * Created by helmi on 10/28/2015.
 */
public class ImagePost extends RealmObject{

    @PrimaryKey
    private String link;
    private String description;

    public ImagePost() {
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
