package com.myfavouritepics.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Description flickr model
 * Created by helmi on 10/30/2015.
 */
public class Description {

    @SerializedName("_content")
    private String _content;

    public Description() {
    }

    public String get_content() {
        return _content;
    }

}
