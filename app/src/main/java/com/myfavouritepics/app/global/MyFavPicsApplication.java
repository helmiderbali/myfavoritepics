package com.myfavouritepics.app.global;

import android.app.Application;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Application class to initiate global instances
 * Created by helmi on 10/29/2015.
 */
public class MyFavPicsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);
    }
}
