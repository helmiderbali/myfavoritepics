package com.myfavouritepics.app.global;

/**
 * A collection of global constants
 * Created by helmi on 10/29/2015.
 */
public class Constants {

    public static final String FLICKER_KEY = "b092f816a7015a9213b40b0738feecb2";
    public static final String THUMBOR_SERVER = "https://thumbfavo.herokuapp.com";
}
