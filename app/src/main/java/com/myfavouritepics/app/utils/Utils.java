package com.myfavouritepics.app.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;

/**
 * Utils class
 * Created by helmi on 11/2/2015.
 */
public class Utils {
    /**
     * Shows a dialog message with title and message
     * @param a activity context
     * @param title dialog title
     * @param message dialog message
     */
    public static void messageDialog(Activity a, String title, String message){
        AlertDialog.Builder dialog = new AlertDialog.Builder(a);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setNeutralButton("OK", null);
        dialog.create().show();

    }

    /**
     * Checks if the nework is working
     * @param context context for check
     * @return is the network availble or not
     */
    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


}
