package com.myfavouritepics.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.myfavouritepics.app.R;
import com.myfavouritepics.app.model.ImagePost;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Adapter for image list loaded from flicker or Thumbor
 * Created by helmi on 10/28/2015.
 */
public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {

    private final List<ImagePost> imagePosts;
    private final Context mContext;

    public ImageListAdapter(List<ImagePost> mPosts, Context mContext) {
        this.imagePosts = mPosts;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item, parent,false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ImagePost post = imagePosts.get(position);
        String description = post.getDescription();

        if (!description.isEmpty()) {
            holder.description.setText(Html.fromHtml(description));
        }
        //load image from url, on fail replace with placeholder
        Picasso.with(mContext)
                .load(post.getLink())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(holder.photo, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.progress.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return imagePosts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView photo;
        public final TextView description;
        public final ProgressBar progress;

        public ViewHolder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.photo);
            description = (TextView) itemView.findViewById(R.id.description);
            progress = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }
}
