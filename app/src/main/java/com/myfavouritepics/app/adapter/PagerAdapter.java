package com.myfavouritepics.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.myfavouritepics.app.ui.fragment.StreamFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Pager adapter that loads correspendant fragment type based on position
 * Created by helmi on 10/28/2015.
 */
public class PagerAdapter extends FragmentPagerAdapter {
    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return StreamFragment.newInstance(position);

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Selfies";
            case 1:
            default:
                return "Travels";
        }
    }

}
