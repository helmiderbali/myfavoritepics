package com.myfavouritepics.app.ui.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import com.myfavouritepics.app.R;
import com.myfavouritepics.app.adapter.PagerAdapter;
import com.myfavouritepics.app.ui.fragment.PhotoDialogFragment;

/**
 * The main acitivty that contain  view pager for photo lists
 */
public class ListActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ImageButton mAddPhotoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        initViews();
        initData();

    }

    /**
     * Initialize the actyvity data
     */
    private void initData() {
        setSupportActionBar(mToolbar);
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoDialogFragment photoDialogFragment = new PhotoDialogFragment();
                photoDialogFragment.show(getSupportFragmentManager(), "photoDialog");
            }
        });
    }

    /**
     * match the views with their variables
     */
    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mAddPhotoButton = (ImageButton) findViewById(R.id.add_photo);
    }

    /**
     * Set up view pager with adapter
     * @param viewPager passed viewpager of fragments
     */
    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

}
