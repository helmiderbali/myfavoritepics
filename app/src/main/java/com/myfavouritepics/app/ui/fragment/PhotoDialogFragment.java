package com.myfavouritepics.app.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.myfavouritepics.app.R;
import com.myfavouritepics.app.listener.AddOnClickListener;
import com.myfavouritepics.app.listener.PhotoOnClickListener;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Gragement used for selfie feature
 * Created by helmi on 10/28/2015.
 */
public class PhotoDialogFragment extends DialogFragment {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int SELECT_PICTURE = 2;
    private static final String TIME_FORMAT = "yyyyMMdd_HHmmss";
    private static final String EXTENSION = ".jpg";
    private static final String PREFIX = "JPEG_";
    private static final String UNDERSCOR = "_";
    public static final String ANDROID_INTENT_EXTRAS_CAMERA_FACING = "android.intent.extras.CAMERA_FACING";
    public ImageView mPhotoView;
    public TextView mComment;
    public Button mAdd;
    private Button mDismiss;
    public String mCurrentPhotoPath;
    private Camera mCamera;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.photo_dialog_fragment, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        mPhotoView = (ImageView) rootView.findViewById(R.id.photo);
        mComment = (TextView) rootView.findViewById(R.id.comment);
        mAdd = (Button) rootView.findViewById(R.id.add);
        mDismiss = (Button) rootView.findViewById(R.id.dismiss);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPhotoView.setOnClickListener(new PhotoOnClickListener(this));
        mAdd.setOnClickListener(new AddOnClickListener(this));

        mDismiss.setOnClickListener(new DismissOnClickListener());
    }


    /**
     * Launch the mCamera to take photo
     */
    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(ANDROID_INTENT_EXTRAS_CAMERA_FACING, 1);
        // Ensure that there's a mCamera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     * Launch library to choose photo from it
     */
    public void dispatchSelectPictureIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, SELECT_PICTURE);
    }

    /**
     * Create an image file and save it to internal memory
     * and save its path for later use
     *
     * @return the image file
     * @throws IOException raised when write or rea disk fail
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault()).format(new Date());
        String imageFileName = PREFIX + timeStamp + UNDERSCOR;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                EXTENSION,         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onPause() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = null;
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = RotateBitmap(bitmap , 90);

        } else if (requestCode == SELECT_PICTURE) {
            Uri imageUri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                Uri tempUri = getImageUri(getActivity(),bitmap);

                mCurrentPhotoPath = getRealPathFromURI(tempUri);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mPhotoView.setImageBitmap(bitmap);

    }


    /**
     * Rotates bitmap from camera
     * @param source source bitmap
     * @param angle angle of rotation
     * @return rotated bitmap
     */
    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * Gets a temporary image uri
     * @param inContext context
     * @param inImage bitmap image
     * @return image uri
     */
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     * get the real storage path from uri
     * @param uri uri to convert
     * @return real path
     */
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String realPath = cursor.getString(idx);
        cursor.close();
        return realPath;
    }


    private class DismissOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            dismiss();
        }
    }
}
