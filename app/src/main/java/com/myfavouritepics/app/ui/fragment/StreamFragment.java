package com.myfavouritepics.app.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.myfavouritepics.app.R;
import com.myfavouritepics.app.Repo.ImagePostRepository;
import com.myfavouritepics.app.adapter.ImageListAdapter;
import com.myfavouritepics.app.global.Constants;
import com.myfavouritepics.app.listener.FlickerListScrollListener;
import com.myfavouritepics.app.listener.FlickrResponseCallback;
import com.myfavouritepics.app.manager.FlickrApiManager;
import com.myfavouritepics.app.model.FlickrResponse;
import com.myfavouritepics.app.model.ImagePost;
import com.myfavouritepics.app.utils.Utils;
import io.realm.RealmResults;
import retrofit.Call;

import java.util.*;

/**
 * Fragment that contains a list of photo either from thumbor or flicker
 */
public class StreamFragment extends Fragment {
    private static final String TAG = StreamFragment.class.getName();
    private static final String FLICKR_PHOTOS_SEARCH = "flickr.photos.search";
    private static final String SEARCH_TERM = "netherlands,nature";
    private static final String SEARCH_TYPE = "json";
    private static final String SEARCH_MAX_RESULT = "20";
    private static final String SEARCH_EXTRA_INFOS = "url_c,description";
    private static final String SEARCH_JSON_CALLBACK = "1";
    public static final int THUMBOR = 0;
    public static final int FLCIKER = 1;

    public RecyclerView mRecyclerView;
    public ImageListAdapter mAdapter;
    public LinearLayoutManager mLayoutManager;
    public SwipeRefreshLayout mSwipeToRefresh;
    public TextView noItems;

    public List<ImagePost> mPosts;

    private int mMode;
    public int mPage = 1;
    public boolean mLoading = true;
    public int mPastVisiblesItems;
    public int mVisibleItemCount;
    public int mTotalItemCount;


    /**
     * Get an instance of fragment
     * @param mode fragment mode thumbor or flicker
     * @return fragment instance
     */
    public static Fragment newInstance(int mode) {
        StreamFragment fragment = new StreamFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("mode", mode);
        fragment.setArguments(bundle);
        return fragment;
    }

    /**
     * Fragment constructor
     */
    public StreamFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stream, container, false);
        rootView.setTag(TAG);
        initViews(rootView);
        initData();
        checkForDataType();
        return rootView;
    }

    /**
     * Check for page data type to process
     */
    private void checkForDataType() {
        if (mMode == THUMBOR) {
            loadImagesFromThumbor();
            configureListForThumbor();
        } else if (mMode == FLCIKER) {
            loadImagesFromFlicker(1);
            ConfigureListForFlickr();
        }
    }

    /**
     * Init members data
     */
    private void initData() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mPosts = new ArrayList<ImagePost>();
        mAdapter = new ImageListAdapter(mPosts, getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mMode = this.getArguments().getInt("mode");
    }

    /**
     * Init views
     * @param rootView parent view
     */
    private void initViews(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mSwipeToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        noItems = (TextView) rootView.findViewById(R.id.no_items);
    }

    /**
     * Configure the list for thumbor photos
     */
    private void configureListForThumbor() {
        mSwipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadImagesFromThumbor();
            }
        });
    }

    /**
     * Configure the list for flickr photos
     */
    private void ConfigureListForFlickr() {
        mSwipeToRefresh.setOnRefreshListener(new FlickerRefreshListener());
        mRecyclerView.setOnScrollListener(new FlickerListScrollListener(this));
    }

    /**
     * Load images from thumbor
     */
    private void loadImagesFromThumbor() {
        ImagePostRepository imageRepo = new ImagePostRepository();
        RealmResults<ImagePost> allPhoto = imageRepo.getAllPhoto();
        if (Utils.isNetworkAvailable(getActivity())) {
            loadImages(allPhoto);
        } else {
            Utils.messageDialog(getActivity(),"Oops !", "Check you internet connection!");
        }
        mSwipeToRefresh.setRefreshing(false);

    }

    /**
     * Load images into the list
     * @param allPhoto photo list
     */
    private void loadImages(RealmResults<ImagePost> allPhoto) {
        if (!allPhoto.isEmpty()) {
            noItems.setVisibility(View.GONE);
            mPosts = new ArrayList<ImagePost>();
            mPosts.addAll(allPhoto);
            Collections.reverse(mPosts);
            mAdapter = new ImageListAdapter(mPosts, getActivity());
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    /**
     * Load image from flicker
     * @param pageNumber page to load
     */
    public void loadImagesFromFlicker(final int pageNumber) {
        Map<String, String> params = new HashMap<String, String>();
        initRequestParams(pageNumber, params);
        Call<FlickrResponse> result = FlickrApiManager.getService().listTravelImages(params);
        result.enqueue(new FlickrResponseCallback(this, pageNumber));
    }

    /**
     * initialize flicker api request params
     * @param pageNumber page number
     * @param params params map
     */
    private void initRequestParams(int pageNumber, Map<String, String> params) {
        params.put("api_key", Constants.FLICKER_KEY);
        params.put("method", FLICKR_PHOTOS_SEARCH);
        params.put("tags", SEARCH_TERM);
        params.put("format", SEARCH_TYPE);
        params.put("per_page", SEARCH_MAX_RESULT);
        params.put("page", String.valueOf(pageNumber));
        params.put("extras", SEARCH_EXTRA_INFOS);
        params.put("nojsoncallback", SEARCH_JSON_CALLBACK);
    }

    /**
     * callback for refresh list listener
     */
    private class FlickerRefreshListener implements SwipeRefreshLayout.OnRefreshListener {
        @Override
        public void onRefresh() {
            mPage = 1;
            mPosts.clear();
            loadImagesFromFlicker(mPage);
        }
    }
}
